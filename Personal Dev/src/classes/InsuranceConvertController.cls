public class InsuranceConvertController {
       
public Client__c newClient = new Client__c();
public Client__c getnewClient(){
return newClient;
}

public boolean displayPopup {get; set;}     
    
    public pagereference closePopup() {        
        displayPopup = false; 
        PageReference secondPage = new PageReference('https://www.google.com');
        secondPage.setRedirect(true);
         return secondPage;
    }     
    public void showPopup() {        
        displayPopup = true;    
    }


public Id ProspectID;
public Insurance_Prospect__c oldProspect{get;set;}
public InsuranceConvertController(ApexPages.StandardController controller){
ProspectID = ApexPages.currentPage().getParameters().get('id');
oldProspect = [Select id, name, Age__c, Email__c, Phone_Mobile__c, Swerz__Converted__c from Insurance_Prospect__c where id =:ProspectID];
if(oldProspect.Swerz__Converted__c == true)
    displayPopup = true;
    else
    displayPopup = false;
}

public PageReference Save() {
//newClient.name= oldProspect.name;
newClient.Email__c= oldProspect.Email__c;
newClient.Mobile_Phone__c= oldProspect.Phone_Mobile__c;
oldProspect.Converted__c = TRUE;
update oldProspect;
insert newClient;
return (new ApexPages.StandardController(newClient)).view();}

}