public class AnoopUserController 
{

    public PageReference cancel() 
    {
        return null;
    }


    public PageReference save() 
    {
        return null;
    }

    public PageReference register() 
    {
        PageReference PageRef = Page.DetailsPage;
        PageRef.setRedirect(true);
        return PageRef;
    }
    public PageReference signIn() 
    {
       PageReference PageRef = Page.SigninPage;
        PageRef.setRedirect(true);
        return PageRef;
    }
}