public class EvaluationController {
Candidate_Skill__c k, k1;

public String getName(){
return 'EvaluationController';}

public String getCandidate() {
k=[select id, Name, Candidate__r.Name from Candidate_Skill__c
where id = :ApexPages.currentPage().getParameters().get('id')];
Return k.Candidate__r.Name;
}

public String getSkill() {
k1=[Select id, Name, Skill__r.Name from Candidate_Skill__c
where id = :ApexPages.currentPage().getParameters().get('id')];
Return k1.Skill__r.Name;
}
}