public class prospectPopupController {
id ProspectID;
Insurance_Prospect__c oldProspect = new Insurance_Prospect__c();
boolean displayPopup {get;set;}
public prospectPopupController(ApexPages.StandardController controller){
ProspectID = ApexPages.currentPage().getParameters().get('id');
oldProspect = [Select id, name, Age__c, Email__c, Phone_Mobile__c, Converted__c from Insurance_Prospect__c where id =:ProspectID];
if(oldProspect.Converted__c == True)
displayPopup = True;
}

}