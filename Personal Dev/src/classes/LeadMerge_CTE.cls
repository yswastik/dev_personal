Public  class LeadMerge_CTE {
Private Lead lead;
List<SelectOption> accountSelectOptions = new List<SelectOption>();
String AccountId;
Public LeadMerge_CTE(ApexPages.StandardController controller) {
            this.lead =[select id, name,company from lead where id =:ApexPages.currentPage().getParameters().get('id')];
}

public List<SelectOption> getAccounts() {
Account[] accounts = [Select Id,Name From Account where Name like :this.lead.Company];  
for(Account a: accounts){
accountSelectOptions.add(new SelectOption(a.Id,a.Name));
}
return accountSelectOptions;
}
public PageReference test() {
return null;
}

public String getAccountId() {
return AccountId;
}

public void setAccountId(String AccountId) {
this.AccountId = AccountId;
}
}