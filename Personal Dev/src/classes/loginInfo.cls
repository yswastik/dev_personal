public class loginInfo {
public login__c LoginDetails = new login__c();
public login__c getLoginDetails() {
        return LoginDetails;
    }

public PageReference Save(){
if(LoginDetails.Password__c != LoginDetails.Confirm_Password__c){
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'The confirm password is not the same as password'));
    return null;

}
insert LoginDetails;
return Null;
 }
}