public class searchOpportunities {
    Account acct;
    public searchOpportunities(){
    this.acct=[select Name from Account where Id= :ApexPages.currentPage().getParameters().get('id') limit 1];
    }
    String acctName=acct.Name;
    Id acctId=acct.Id;
    Opportunity[] opties;
    public String getacctName(){
    return acctName;
    }
    public Opportunity[] getOpties(){
      return opties;
    }
    public PageReference search(){
    opties =[select Name from Opportunity where Account.Name = :acctName];
    return null;
    }
}