global Class Utilities{

Public static Boolean IsInSalesTeam(Id OpportunityId,Id OwnerId,OpportunityTeamMember[ ] salesTeamMembers)
{
Boolean result =false;
for(OpportunityTeamMember otm : salesTeamMembers)
{
if(otm.OpportunityId==OpportunityId && otm.UserId ==OwnerId){result =true; break;}
}
return result;
}
   
Public static Boolean IsInOpportunityShare(Id OpportunityId,Id OwnerId,OpportunityShare[ ] opptyShareMembers)
{
Boolean result =false;
for(OpportunityShare os : opptyShareMembers)
{
if(os.OpportunityId==OpportunityId && os.UserOrGroupId ==OwnerId){result =true; break;}
}
return result;
}

Public WebService Static String checkPartnerAccount(String PartnerID)
{
String k='1';
Account[] PartnerIds=[Select Id From Account where Partner_ID__c =:PartnerID and RecordTypeId = '012400000009eCa' limit 2];
if(PartnerIds.size()==0){k='0';}
if(PartnerIds.size()==2){k='2';}
return k;
}

Public static List<Id> getReverseMapArray(Id id,Map<Id,Id> MapToReverse)
{
List<Id> reverseArray = new List<Id>();
   for(Id i : MapToReverse.keyset())
   {
      if(id==MapToReverse.get(i))
       {
       reverseArray.Add(i);
       }
   }
return reverseArray;
}
   
}