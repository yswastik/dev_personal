public class anoopsigninpage 
{
 
    private String userid;
    private String password;
    
    public String getUserid() 
    {
       return this.userid;
    }

    public void setUserid(String userid) 
    {
       this.userid = userid;
    } 
    
    public String getPassword() 
    {
       return this.password;
    }
    public void setPassword(String password) 
    {
       this.password = password;
    } 

    public PageReference login() 
    {
        Integer i = [select count() from user_detail__c  where userid__c=:this.Userid and pwd__c=:this.Password];
        if(i==1)
        {
        PageReference PageRef = Page.poc1;
        PageRef.setRedirect(true);
        return PageRef;
        }
        else
        {
        PageReference PageRef = Page.anoop_userpage;
        PageRef.setRedirect(true);
        return PageRef;
        }
    }
    
    public PageReference cancel() 
    {
        PageReference PageRef = Page.anoop_userpage;
        PageRef.setRedirect(true);
        return PageRef;
    }
}