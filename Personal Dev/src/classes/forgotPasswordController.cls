public class forgotPasswordController {

    public PageReference SendEmail() {
     Login__c LoginUser=[Select id, name, Email_ID__c, Password__c from Login__c where name =:UserName limit 1];
     String[] toAddresses= new String[]{LoginUser.Email_ID__c};
     Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();
     mail.setToAddresses(toAddresses);
     mail.setSubject('Your password is enclosed');
     mail.setPlaintextBody('Your password is ' + LoginUser.Password__c);
     Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        return null;
    }
    public String UserName { get; set; }
}