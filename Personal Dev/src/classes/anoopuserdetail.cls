public class anoopuserdetail
{
    private String firstName;
    private String lastName;
    private String userid;
    private String password;
    
    public String getFirstName() 
    {
        return this.firstName;
    }
    public void setFirstName(String firstName) 
    {
        this.firstName = firstName;
    }
    public String getLastName() 
    {
        return this.lastName;
    }
    public void setLastName(String lastName) 
    {
        this.lastName = lastName;
    } 
     public String getUserid() 
    {
        return this.userid;
    }
    public void setUserid(String userid) 
    {
        this.userid = userid;
    } 
    
    public String getPassword() 
    {
        return this.password;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    } 
    public PageReference Save() 
    {
        user_detail__c userdetail = new user_detail__c(Last_Name__c=this.LastName,
        First_Name__c=this.FirstName,
        pwd__c=this.Password,
        userid__c=this.Userid);
        insert userdetail;
        PageReference PageRef = Page.anoop_userpage;
        PageRef.setRedirect(true);
        return PageRef;
    }

    public PageReference Cancel() 
    {
       PageReference PageRef = Page.anoop_userpage;
        PageRef.setRedirect(true);
        return PageRef;
    }
}