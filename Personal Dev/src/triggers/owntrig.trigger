trigger owntrig on Account (before update)
{
Account[] oa = Trigger.old;
Account[] na = Trigger.new;
if(na[0].OwnerId!=oa[0].OwnerId)
{
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

String[] toAddresses = new String[] {'tcs.salesforce@gmail.com'};
mail.setToAddresses(toAddresses);
mail.setPlainTextBody('OWNER CHANGE');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
}