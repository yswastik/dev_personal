trigger acctrigger on Account (after update) 
{

Account[] oldAccounts = Trigger.old;
Account[] newAccounts = Trigger.new;
if(newAccounts[0].BillingCity!=oldAccounts[0].BillingCity || newAccounts[0].BillingCountry!= oldAccounts[0].BillingCountry || newAccounts[0].BillingState!=oldAccounts[0].BillingState || newAccounts[0].BillingStreet!=oldAccounts[0].BillingStreet || newAccounts[0].BillingPostalCode!=oldAccounts[0].BillingPostalCode)
{
Contact[] c=[select Id, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry from Contact Where AccountId = :newAccounts[0].Id];
for (Integer i=0;i<c.size();i++)
{
    c[i].MailingCity=newAccounts[0].BillingCity;
    c[i].MailingStreet=newAccounts[0].BillingStreet;
    c[i].MailingPostalCode=newAccounts[0].BillingPostalCode;
    c[i].MailingState=newAccounts[0].BillingState;
    c[i].MailingCountry=newAccounts[0].BillingCountry;
}

update c;
}
}