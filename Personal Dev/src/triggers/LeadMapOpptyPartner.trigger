trigger LeadMapOpptyPartner on Lead bulk (after update) {

/* 
This trigger does following
If Lead has Partner User ID then:
1. Updates the Partner Name of the converted Oppty with the Channel Account Name which has that value.
2. Checks for the Contact on the Channel Account found in step 1 and wherein Email = Partner Email on the converted lead.
   If contact is found, adds it to the Converted Oppty as a Contact Roles. Else creates the Contact on that Channel Account and then adds it to the Converted Oppty as a Contact Role.
3. Updates the role for the default contact role of oppty as Bussiness User
*/


List<Lead> leadsToProcess = new List<Lead>();
List<Opportunity> opptysToUpdate =new List<Opportunity>();
List<Contact> contactsToAdd = new List<Contact>();
List<OpportunityContactRole> conatctRolesToAdd = new List<OpportunityContactRole>();
List<Lead> leadWithoutPartnerContacts =new List<Lead>();
Set<String> partnerIDs =new Set<String>();
Set<Id> opptyIds =new Set<Id>();
List<OpportunityContactRole> conatctRolesToUpdate = new List<OpportunityContactRole>();

for(Lead l : System.Trigger.New)
{
    if(l.isConverted==true)//processing only converted leads among all updated leads
    {
    leadsToProcess.add(l);
    opptyIds.add(l.ConvertedOpportunityId);
    if(l.Partner_User_ID__c!=null){partnerIDs.add(l.Partner_User_ID__c);}//adding Partner User ID  of lead to partnerIDs set
    }
}
//Update default Contact Role
for(OpportunityContactRole ocr : [Select Role,Id from OpportunityContactRole where OpportunityId IN :opptyIds])
{
OpportunityContactRole ocr1 =new OpportunityContactRole(Id=ocr.Id);
ocr1.Role='Business User';
conatctRolesToUpdate.add(ocr1);
}






//Create Map of Id to Account fro all Partner Accounts with PartnerID in all converted lead PartnerIDs 
Map<Id,Account> mappedPartners = new Map<Id,Account>([select Partner_ID__c from Account where RecordTypeId='012400000009eCa' and Partner_ID__c IN :partnerIDs]);


//Create map of PartnerID to Account
Map<String,Id>  mappedPartnerIDs = new Map<String,Id>();

for(Account a : mappedPartners.values())
{
mappedPartnerIDs.put(a.Partner_ID__c,a.Id);
}


//Create map of Id to contact for contacts with account Id in all above partner Accounts
Map<Id,Contact> mappedContacts = new Map<Id,Contact>([select Id,Name,FirstName,LastName,AccountId,Email from Contact where AccountId IN :mappedPartnerIDs.values()]);
//create map fro Contact id to Account Id
Map<Id,Id>  mappedContactIdAccountId = new Map<Id,Id>();
for(Contact a : mappedContacts.values())
{
mappedContactIdAccountId.put(a.Id,a.AccountId);
}

//Create map for account id to Contact Ids array for that account.
Map<Id,List<Id>> mappedAccountIdContactIds = new Map<Id,List<Id>>();
for(Id accountId : mappedPartners.keyset())
{
mappedAccountIdContactIds.put(accountId,Utilities.getReverseMapArray(accountId,mappedContactIdAccountId));
}



for(Lead l :leadsToProcess)
{
    //update Partner Name on Oppty
    if(l.Partner_User_ID__c!=null){    
                    Opportunity o= new Opportunity(Id=l.ConvertedOpportunityId);
                    o.Partner_Account__c= mappedPartnerIDs.get(l.Partner_User_ID__c);
                    opptysToUpdate.add(o);
                             

//Search for the contact with account as  partner account of lead and email as partner email of lead
    
Id[] partnerContactsIds = mappedAccountIdContactIds.get(mappedPartnerIDs.get(l.Partner_User_ID__c));

  Boolean flag = false;
     //if contact is found then add it to oppty contact roles
    if(partnerContactsIds.size()>0)
    {
      
       for(Id c:partnerContactsIds)
        {
            if(mappedContacts.get(c).Email==l.Partner_Email__c)
            {
            flag=true;
            OpportunityContactRole ocr =new OpportunityContactRole();
            ocr.ContactId=c;
            ocr.OpportunityId=l.ConvertedOpportunityId;
            ocr.Role='Partner';
            conatctRolesToAdd.add(ocr);
            break;
            }
        }  
     }        
        
        //create contact if not present
    if(!flag){    
    Contact c =new Contact();
    c.FirstName=l.Partner_Contact_First_Name__c;
    c.LastName=l.Partner_Contact_Last_Name__c;
    c.AccountId=mappedPartnerIDs.get(l.Partner_User_ID__c);
    c.Email=l.Partner_Email__c;
    contactsToAdd.add(c);
    leadWithoutPartnerContacts.add(l);// add lead to process again for addtion of contact role to oppty
     }
      
   }

}
//insert contacts to database
if(contactsToAdd.size()>0){Database.SaveResult[] result = Database.Insert(contactsToAdd);}
   
//update maps again
mappedContacts = new Map<Id,Contact>([select Id,Name,FirstName,LastName,AccountId,Email from Contact where AccountId IN :mappedPartnerIDs.values()]);
for(Contact a : mappedContacts.values())
{
mappedContactIdAccountId.put(a.Id,a.AccountId);
}
for(Id accountId : mappedPartners.keyset())
{
mappedAccountIdContactIds.put(accountId,Utilities.getReverseMapArray(accountId,mappedContactIdAccountId));
}


   //process leads whose partner contact was not present for addition of contact role 
   for(Lead l:leadWithoutPartnerContacts)
   {
   
       Id[] partnerContactsIds = mappedAccountIdContactIds.get(mappedPartnerIDs.get(l.Partner_User_ID__c));

       for(Id c:partnerContactsIds)
        {
         if(mappedContacts.get(c).Email==l.Partner_Email__c)
            {
            OpportunityContactRole ocr =new OpportunityContactRole();
            ocr.ContactId=c;
            ocr.OpportunityId=l.ConvertedOpportunityId;
            ocr.Role='Partner';
            conatctRolesToAdd.add(ocr);
            break;
            }
        }  
   }

if(opptysToUpdate.size()>0){Update opptysToUpdate;} //update opptys in the database.
if(conatctRolesToUpdate.size()>0){Update conatctRolesToUpdate;}
if(conatctRolesToAdd.size()>0){Insert conatctRolesToAdd;}//insert contact roles in the database.
}