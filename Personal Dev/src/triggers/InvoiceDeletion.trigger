trigger InvoiceDeletion on Invoice_Statement__c (before delete) {
    //Invoice_Statement__c[] Invwith = new list<Invoice_Statement__c>();
    for(Invoice_Statement__c Invs: [Select id from Invoice_Statement__c where ID in (Select Invoice_Statement__c from Line_Item__c) and ID in :Trigger.old]){
    Trigger.oldMap.get(Invs.Id).adderror('Cannot delete this record');        
        }
}